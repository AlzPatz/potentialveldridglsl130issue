using System.Numerics;

namespace potentialissue
{
    struct VertexPosition
    {
        public Vector2 Position;

        public VertexPosition(Vector2 position)
        {
            Position = position;
        }
        public const uint SizeInBytes = 8;
    }
}