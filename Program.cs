﻿using System;
using System.IO;
using System.Numerics;
using Veldrid;
using Veldrid.Sdl2;
using Veldrid.StartupUtilities;

namespace potentialissue
{
    class Program
    {
        static void Main(string[] args)
        {
            if(Initialise())
                Loop();
        }
            
        private static Sdl2Window _window;
        private static GraphicsDevice _graphicsDevice;
        private static ResourceFactory _resourceFactory;
        private static CommandList _commandList;
        private static DeviceBuffer _testUniformBuffer;
        private static DeviceBuffer _vertexBuffer;
        private static DeviceBuffer _indexBuffer;
        private static Shader _vertexShader;
        private static Shader _fragmentShader;
        private static ResourceSet _uniformSet;
        private static Pipeline _pipeline;

        private static bool Initialise()
        {
            //Create Window
            WindowCreateInfo windowCI = new WindowCreateInfo()
            {
                X = 100,
                Y = 100,
                WindowWidth = 960,
                WindowHeight = 540,
                WindowTitle = "Veldrid Tutorial"
            };

            _window = VeldridStartup.CreateWindow(ref windowCI);

            _graphicsDevice = VeldridStartup.CreateGraphicsDevice(_window); 

            _resourceFactory = _graphicsDevice.ResourceFactory;

            if(_graphicsDevice.BackendType != GraphicsBackend.OpenGL)
            {
                Console.WriteLine("This example requires OpenGL backend");
                return false;
            }

            //RELEVENT CODE ========================================================================================
            _testUniformBuffer = _resourceFactory.CreateBuffer(new BufferDescription(16, BufferUsage.UniformBuffer));
            //RELEVENT CODE ========================================================================================

            VertexPosition[] vertices =
            {
                new VertexPosition(new Vector2(-0.5f, 0.5f)),
                new VertexPosition(new Vector2(0.5f, 0.5f)),
                new VertexPosition(new Vector2(0.5f, -0.5f)),
                new VertexPosition(new Vector2(-0.5f, -0.5f))
            };

            ushort[] indices = { 0, 1, 2, 0 , 2, 3 };

            _vertexBuffer = _resourceFactory.CreateBuffer(new BufferDescription(4 * VertexPosition.SizeInBytes, BufferUsage.VertexBuffer));
            _indexBuffer = _resourceFactory.CreateBuffer(new BufferDescription(6 * sizeof(ushort), BufferUsage.IndexBuffer));

            _graphicsDevice.UpdateBuffer(_vertexBuffer, 0, vertices);
            _graphicsDevice.UpdateBuffer(_indexBuffer, 0, indices);

            _vertexShader = LoadShader(ShaderStages.Vertex);
            _fragmentShader = LoadShader(ShaderStages.Fragment);  

            var perVertexLayout = new VertexLayoutDescription(new VertexElementDescription("Position", VertexElementSemantic.Position, VertexElementFormat.Float2));

            ShaderSetDescription shaderSet = new ShaderSetDescription(
                new[]
                {
                    perVertexLayout
                },
                new[]
                {
                    _vertexShader, _fragmentShader
                });

            //RELEVENT CODE ========================================================================================
            ResourceLayout testUniformLayout = _resourceFactory.CreateResourceLayout(
                new ResourceLayoutDescription(
                    new ResourceLayoutElementDescription("TestUniform", ResourceKind.UniformBuffer, ShaderStages.Vertex)
                )
            );
            //RELEVENT CODE ========================================================================================

            _pipeline = _resourceFactory.CreateGraphicsPipeline(new GraphicsPipelineDescription(
                BlendStateDescription.SingleOverrideBlend,
                DepthStencilStateDescription.DepthOnlyLessEqual,
                RasterizerStateDescription.Default,
                PrimitiveTopology.TriangleList,
                shaderSet,
                new[] { testUniformLayout  },
                _graphicsDevice.SwapchainFramebuffer.OutputDescription
            ));

            //RELEVENT CODE ========================================================================================
            _uniformSet = _resourceFactory.CreateResourceSet(new ResourceSetDescription(
                testUniformLayout,
                _testUniformBuffer));
            //RELEVENT CODE ========================================================================================

            _commandList = _resourceFactory.CreateCommandList();

            return true;
        }

        private static Shader LoadShader(ShaderStages stage)
        {
            string extension = null;
            switch (_graphicsDevice.BackendType)
            {
                case GraphicsBackend.Direct3D11:
                    extension = "hlsl.bytes";
                    break;
                case GraphicsBackend.Vulkan:
                    extension = "spv";
                    break;
                case GraphicsBackend.OpenGL:
                    extension = "glsl";
                    break;
                default: throw new System.InvalidOperationException();
            }

            string entryPoint = stage == ShaderStages.Vertex ? "VS" : "FS";
            string path = Path.Combine(System.AppContext.BaseDirectory, "Shaders", $"{stage.ToString()}.{extension}");
            byte[] shaderBytes = File.ReadAllBytes(path);

            return _graphicsDevice.ResourceFactory.CreateShader(new ShaderDescription(stage, shaderBytes, entryPoint));
        }

        private static void Loop()
        {
            while(_window.Exists)
            {
                _window.PumpEvents();
                Draw();
            }

            ReleaseResources();
        }

        private static void Draw()
        {
            _commandList.Begin();
            //RELEVENT CODE ========================================================================================
            var uniform = new float[]{1.0f, 1.0f, 1.0f, 1.0f };
            _commandList.UpdateBuffer(_testUniformBuffer, 0, uniform);
            //RELEVENT CODE ========================================================================================
            _commandList.SetFramebuffer(_graphicsDevice.SwapchainFramebuffer);
            _commandList.ClearColorTarget(0, RgbaFloat.Pink);
            _commandList.SetVertexBuffer(0, _vertexBuffer);
            _commandList.SetIndexBuffer(_indexBuffer, IndexFormat.UInt16);
            _commandList.SetPipeline(_pipeline);
            _commandList.SetGraphicsResourceSet(0, _uniformSet);
            _commandList.DrawIndexed(6, 1, 0, 0, 0);
            _commandList.End();
            _graphicsDevice.SubmitCommands(_commandList);
            _graphicsDevice.SwapBuffers();
            _graphicsDevice.WaitForIdle();
        }

        public static void ReleaseResources()
        {
            _graphicsDevice.Dispose();
            _commandList.Dispose();
            _vertexBuffer.Dispose();
            _indexBuffer.Dispose();
            _vertexShader.Dispose();
            _fragmentShader.Dispose();
            _pipeline.Dispose();
        }
    }
}