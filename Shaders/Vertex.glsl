#version 320 es

precision highp float;

uniform TestUniform
{
    vec4 test_uniform;
};

in vec2 Position;

out vec4 Color;

void main()
{
    Color = test_uniform;

    gl_Position = vec4(Position.x, Position.y, 0.0, 1.0f);
}