#version 320 es

precision highp float;

in vec4 Color;

out vec4 fragColor;

void main()
{
    fragColor = Color;
}